package com.ichsan.eappointmenteabsent.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.EditText;
import android.widget.Toast;

import com.ichsan.eappointmenteabsent.R;
import com.ichsan.eappointmenteabsent.utility.SessionManager;

import java.util.ConcurrentModificationException;

public class MainActivity extends AppCompatActivity {

    private Context context;
    private EditText uid,pass;
    private Button login;
    private CheckBox remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context= this;
        uid = findViewById(R.id.uid);
        pass = findViewById(R.id.pass);
        login = findViewById(R.id.button);
        remember = findViewById(R.id.remember);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uid.getText().toString().length() > 0 && pass.getText().toString().length() > 0){
                    //////// Action to login/////////////
                    if(remember.isChecked()){
                        SessionManager.setDataLogin(context,
                                uid.getText().toString(),
                                remember.isChecked(),
                                "1",
                                pass.getText().toString());
                    }
                }else{
                    Toast.makeText(context, "Mohon inputkan User dan Password Anda !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}