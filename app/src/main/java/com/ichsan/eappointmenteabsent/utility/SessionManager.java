package com.ichsan.eappointmenteabsent.utility;


import android.content.Context;
import android.content.SharedPreferences;

import com.ichsan.eappointmenteabsent.constant.Constanta;

public class SessionManager {

    //konstruktor untuk menggunakan shared preferences
    protected static SharedPreferences retrieveSharedPreferences(Context context) {
        return context.getSharedPreferences(Constanta.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    //untuk CRUD
    protected static SharedPreferences.Editor retrieveSharedPreferencesEditor(Context context)
    {
        return retrieveSharedPreferences(context).edit();
    }

    //procedure sesuai kebutuhan
    public static void setDataLogin(Context context,
                                    String uname,
                                    Boolean remember,
                                    String role,
                                    String Password){

        SharedPreferences.Editor editor = retrieveSharedPreferencesEditor(context);

        editor.putString(Constanta.UNAME,uname);
        editor.putBoolean(Constanta.REMEMBER,remember);
        editor.putString(Constanta.ROLE,role);
        editor.putString(Constanta.PASS,Password);
        editor.putBoolean(Constanta.LOGIN_FLAG,true);
        editor.commit();
    }

    //cek flag login
    public static boolean cekLoginFlag(Context context)
    {
        return retrieveSharedPreferences(context).getBoolean(Constanta.LOGIN_FLAG,false);
    }

    //ambil uname
    public static String getUname(Context context)
    {
        return retrieveSharedPreferences(context).getString(Constanta.UNAME,"");
    }
    //ambil password
    public static String getPass(Context context)
    {
        return retrieveSharedPreferences(context).getString(Constanta.PASS,"");
    }
    //cek remember
    public static Boolean cekRemember(Context context)
    {
        return retrieveSharedPreferences(context).getBoolean(Constanta.REMEMBER,false);
    }
    //logout
    public static void clearData(Context context)
    {
        SharedPreferences.Editor editor = retrieveSharedPreferencesEditor(context);

        if(cekRemember(context)){
            editor.putBoolean(Constanta.LOGIN_FLAG, false);
        }
        else {
            editor.clear();
        }
        editor.commit();
    }

    public static void setRole(Context context, String role) {
        SharedPreferences.Editor editor = retrieveSharedPreferencesEditor(context);
        editor.putString(Constanta.ROLE,role);
        editor.commit();
    }

    public static String getRole(Context context){
        return retrieveSharedPreferences(context).getString(Constanta.ROLE,"");
    }



}
